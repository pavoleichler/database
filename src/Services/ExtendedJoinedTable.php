<?php

namespace PavolEichler\Database;

/**
 * Base class for an object providing access to data stored in several SQL tables.
 * Several fetched rows may describe one item.
 *
 * @author Pavol Eichler <pavol.eichler@gmail.com>
 */
abstract class ExtendedJoinedTable extends JoinedTable
{


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     *                             Model behaviour
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Extends the given fluent select with additional data from joined tables.
     * You can create a complex join, which will retrieve several rows for each object.
     * Then you would parse the results in the fetch() method, e.g. using the dibi::fetchAssoc().
     * As extend is alwyas called only after the limit, offset and order parameters have been applied, it will not interfere with paging.
     *
     * @param \Dibi\Fluent $fluent
     * @return \Dibi\Fluent
     */
    protected function extend(\Dibi\Fluent $fluent) {

        return $this->dibi->select('*')->from($fluent, $this->table(self::TABLE_MAIN, self::FORMAT_ALIAS));

    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     *                             Selects
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Returns the extended basic select.
     * Call without arguments to retrieve the default fields, or pass custom fields in the same way, you would call the dibi::select() method.
     *
     * @return \Dibi\Fluent
     */
    protected function extended() {

        $args = func_get_arg();
        $select = call_user_func_array(array($this, 'select'), $args);

        $fluent = $this->adjustDibiBehaviour(null, function() use ($select) {
            return $this->extend($select);
        });
        
        return $fluent;

    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     *                                 Tools
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Fetches all rows.
     *
     * @param \Dibi\Fluent $fluent
     * @return \Dibi\Result The returned rows.
     */
    protected function fetchAll(\Dibi\Fluent $fluent) {

        $fluent = $this->extend($fluent);

        return parent::fetchAll($fluent);
    }

}